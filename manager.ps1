#!/usr/local/bin/pwsh
# requires -version 7

param (
    # Селектор команд
    [Parameter (Mandatory = $true, Position = 1)] [string] $command,
    # Переменные создаваемой виртуальной машины
    [string] $vm_name,
    [int64] $vm_ram_size,
    [int64] $vm_cpu_count,
    [int64] $vm_storage_size
)
# Константы
$CONFIGS_DIR = [IO.Path]::Combine($PSScriptRoot, "configs")

# Загружаем модули
try {
    . ([IO.Path]::Combine($PSScriptRoot, 'modules', 'hyper-v.ps1'))
    . ([IO.Path]::Combine($PSScriptRoot, 'modules', 'downloads.ps1'))
    . ([IO.Path]::Combine($PSScriptRoot, 'modules', 'yaml-loader.ps1'))
}
catch {
    Write-Host "- Ошибка при загрузке модулей" -ForegroundColor Red
}

# Читаем файлы конфигурации
$defaults = Load-Configuration defaults.yaml
$hyperv = Load-Configuration hyperv.yaml

# Определяем пути
try {
    $vm_source = $config.paths.vm_source
    $vm_root = $config.paths.vm_root
    $iso_root_path = $config.paths.iso
    $vhd_root_path = $config.paths.vhd
}
catch {
    Write-Host "- Ошибка при определении путей" -ForegroundColor Red
}

# Обработчик селектора команд
switch ($command) {
    "/state" {
        Get-WindowsOptionalFeature -Online -FeatureName *hyper*
    }

    "/download" {
        foreach ($url in $config.iso) {
            Get-File -url $url -path $iso_root_path
        }
    }

    "/create" {
        foreach ($vm in $hyperv.vm_list) { New-VirtualMachine $vm }
    }

    "/get" {
        Get-VM
    }

    "/update" {}

    "/remove" {}

    "/help" { Write-Host "Справка по скрипту: README.md" -ForegroundColor Yellow }
    Default { Write-Host "Произошло что-то не то, попробуйте: ./manager.ps1 /help или почитать readme.md" -ForegroundColor Red }
}

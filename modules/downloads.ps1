# requires -version 7
Write-Host "+ Модуль загружен: downloads" -ForegroundColor Green

function Get-File {
    param (
        [string] $url,
        [string] $path
    )
    try {
        $uotputFile = Split-Path $url -Leaf
        $out_path = [IO.Path]::Combine($path, $uotputFile)
    
        if (![System.IO.File]::Exists($out_path)) {
            $webClient = New-Object System.Net.WebClient
            $webClient.DownloadFile($url, $out_path)
    
            Write-Host "  > Файл $uotputFile успешно скачан." -ForegroundColor Green
        }
        else {
            Write-Host "  > Файл $uotputFile уже существует." -ForegroundColor Yellow
        }
    
    }
    catch {
        Write-Host $uotputFile ":" $_.Exception.Message -ForegroundColor Red
    }
}

# requires -version 7
Write-Host "+ Модуль загружен: hyper-v" -ForegroundColor Green

function New-VirtualSwitch {
    param ( [System.Object] $virtualswitch )
    
    $virtualswitch_is_exists = Get-VMSwitch -Name $virtualswitch.name -ErrorAction SilentlyContinue
    if($null -eq $virtualswitch_is_exists.Name) {
        New-VMSwitch `
            -Name $virtualswitch.name `
            -Notes $virtualswitch.notes `
            -NetAdapterName $virtualswitch.brige | Out-Null
        Write-Host "  > Виртаульный сетевой адаптер $($virtualswitch.name) успешно создана." -ForegroundColor Green
        return $virtualswitch.name
    } else {
        Write-Host "  > Виртаульный сетевой адаптер $($virtualswitch.name) уже существует." -ForegroundColor Yellow
        return $virtualswitch.name
    }
}

function New-VirtualDisk {
    param ( 
        [System.Object] $virtualdisk,
        [string] $name
    )

    $storage_path = [IO.Path]::Combine($defaults.paths.vhd_storage, "$($name).vhdx")
    $virtualdisk_is_exists = Get-VHD -Path $storage_path -ErrorAction SilentlyContinue
    if ($null -eq $virtualdisk_is_exists) {
        switch ($virtualdisk.type) {

            "fixed" { New-VHD -Path $storage_path -Fixed -SizeBytes $virtualdisk.size | Out-Null }
            "dynamic" { New-VHD -Path $storage_path -Dynamic -SizeBytes $virtualdisk.size | Out-Null }
            # "differencing" { New-VHD -Path $storage_path -Differencing -SizeBytes $virtualdisk.size | Out-Null }

            Default { New-VHD -Path $storage_path -SizeBytes $virtualdisk.size }
        }
        Write-Host "  > Виртаульный диск $($storage_path) успешно создан." -ForegroundColor Green
        return $storage_path
    } else {
        Write-Host "  > Виртаульный диск $($storage_path) уже существует." -ForegroundColor Yellow
        return $storage_path
    }
}

function New-VirtualMachine {
    param ( [System.Object] $vm )

    $vm_is_exists = Get-VM -Name $vm.name -ErrorAction SilentlyContinue
    if ($null -eq $vm_is_exists.Name) {

        # Определяем поколение ВМ
        if ($null -eq $vm.generation) {
            $generation = $hyperv.defaults.generation
        } else {
            $generation = $vm.generation
        }

        $vswitch_name = New-VirtualSwitch $vm.network
        $vhdx_path = New-VirtualDisk $vm.storage $vm.name

        New-VM `
            -Name $vm.name `
            -Generation $generation `
            -VHDPath $vhdx_path `
            -Switchname "$vswitch_name" | Out-Null

        if ($null -eq $vm.memory) {
            $memory_type = $hyperv.defaults.memory.type
            $memory_size = $hyperv.defaults.memory.size
        } else {
            $memory_type = $vm.memory.type
            $memory_size = $vm.memory.size
        }

        if ($null -eq $vm.cpu_count) {
            $cpu_count = $hyperv.defaults.cpu_count
        } else {
            $cpu_count = $vm.cpu_count
        }

        if ($memory_type -eq "dynamic") {
            Set-VM `
                -VMName $vm.name `
                -ProcessorCount $cpu_count `
                -DynamicMemory `
                -MemoryMaximumBytes $memory_size
        } elseif ($memory_type -eq "static") {
            Set-VM `
                -VMName $vm.name `
                -ProcessorCount $cpu_count `
                -StaticMemory `
                -MemoryMaximumBytes $memory_size
        }

        $iso_path = [IO.Path]::Combine($defaults.paths.iso_storage, $hyperv.defaults.dvd.iso)
        Add-VMDvdDrive `
            -VMName $vm.name `
            -Path $iso_path

        Set-VMFirmware `
            -VMName $vm.name `
            -EnableSecureBoot On `
            -SecureBootTemplate "MicrosoftUEFICertificateAuthority"

        Write-Host "  > VM $name успешно создана." -ForegroundColor Green
    } else {
        Write-Host "  > VM $name уже существует." -ForegroundColor Yellow
    }
}

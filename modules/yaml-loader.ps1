# requires -version 7
Write-Host "+ Модуль загружен: yaml-loader" -ForegroundColor Green

function Load-Configuration {
    param (
        [string] $filename
    )
    try {
        $configurationFile = [IO.Path]::Combine($CONFIGS_DIR ,$filename)
        if ([System.IO.File]::Exists($configurationFile)) {
            $config = Get-Content $configurationFile | Out-String | ConvertFrom-Yaml
            if($config.version) {
                Write-Host "+ Файл конфигурации $filename прочитан, версия:" $config.version -ForegroundColor Green
                return $config
            }
            else {
                Write-Host "- Ошибка при обработке файла конфигурации ${filename}: файл не валиден" -ForegroundColor Red
            }
        }
        else {
            Write-Host "- Ошибка при обработке файла конфигурации ${filename}: файл не найден" -ForegroundColor Red
        }
    }
    catch {
        Write-Host "- Ошибка при обработке файла конфигурации ${filename}: необработанное исключение" -ForegroundColor Red
    }
}